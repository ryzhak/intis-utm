import { IntisUtmPage } from './app.po';

describe('intis-utm App', function() {
  let page: IntisUtmPage;

  beforeEach(() => {
    page = new IntisUtmPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
